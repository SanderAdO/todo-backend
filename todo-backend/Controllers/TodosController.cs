﻿using MongoDBWebAPI.Models;
using MongoDBWebAPI.Services;
using Microsoft.AspNetCore.Mvc;

namespace MongoDBWebAPI.Controllers;

[ApiController]
[Route("api/todos")]
public class TodosController : ControllerBase
{
    private readonly TodosService _todosService;

    public TodosController(TodosService todosService) =>
        _todosService = todosService;

    [HttpGet]
    public async Task<List<Todo>> Get() =>
        await _todosService.GetAsync();

    [HttpGet("{title}")]
    public async Task<ActionResult<Todo>> Get(string title)
    {
        var todo = await _todosService.GetAsync(title);

        if (todo is null)
        {
            return NotFound();
        }

        return todo;
    }

    [HttpPost]
    public async Task<IActionResult> Post(Todo newTodo)
    {

        await _todosService.CreateAsync(newTodo);

        return CreatedAtAction(nameof(Get), new { id = newTodo.id }, newTodo);
    }

    [HttpPut("{title}")]
    public async Task<IActionResult> Update(string title, Todo updatedTodo)
    {
        var todo = await _todosService.GetAsync(title);

        if (todo is null)
        {
            return NotFound();
        }

        updatedTodo.id = todo.id;

        await _todosService.UpdateAsync(title, updatedTodo);

        return NoContent();
    }

    [HttpDelete("{title}")]
    public async Task<IActionResult> Delete(string title)
    {
        var todo = await _todosService.GetAsync(title);

        if (todo is null)
        {
            return NotFound();
        }

        await _todosService.RemoveAsync(todo.title);

        return NoContent();
    }
}