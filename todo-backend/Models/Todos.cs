﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace MongoDBWebAPI.Models
{
    public class Todo
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string? id { get; set; }
        public string? title { get; set; } = null!;
        public string? subject { get; set; } = null!;
        public string? description { get; set; } = null!;
        public string? deadline { get; set; } = null!;
    }
}
