﻿using MongoDBWebAPI.Models;
using Microsoft.Extensions.Options;
using MongoDB.Driver;

namespace MongoDBWebAPI.Services
{
    public class TodosService
    {
        private readonly IMongoCollection<Todo> _todosCollection;

        public TodosService(
            IOptions<TodoStoreDatabaseSettings> todoStoreDatabaseSettings)
        {
            var mongoClient = new MongoClient(
                todoStoreDatabaseSettings.Value.ConnectionString);

            var mongoDatabase = mongoClient.GetDatabase(
                todoStoreDatabaseSettings.Value.DatabaseName);

            _todosCollection = mongoDatabase.GetCollection<Todo>(
                todoStoreDatabaseSettings.Value.TodosCollectionName);
        }

        public async Task<List<Todo>> GetAsync() =>
            await _todosCollection.Find(_ => true).ToListAsync();

        public async Task<Todo?> GetAsync(string title) =>
            await _todosCollection.Find(x => x.title == title).FirstOrDefaultAsync();

        public async Task CreateAsync(Todo newTodo) =>
            await _todosCollection.InsertOneAsync(newTodo);

        public async Task UpdateAsync(string title, Todo updatedTodo) =>
            await _todosCollection.ReplaceOneAsync(x => x.title == title, updatedTodo);

        public async Task RemoveAsync(string title) =>
            await _todosCollection.DeleteOneAsync(x => x.title == title);
    }
}
