using Microsoft.EntityFrameworkCore;
using MongoDB.Driver;
using MongoDB.Bson;
using Newtonsoft.Json;
using todo_backend;
using MongoDB.Bson.Serialization.Attributes;
using Microsoft.Extensions.Options;
using Microsoft.AspNetCore.Mvc;

using MongoDBWebAPI.Models;
using MongoDBWebAPI.Services;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container
builder.Services.Configure<TodoStoreDatabaseSettings>(
    builder.Configuration.GetSection("TodoStoreDatabase"));

builder.Services.AddSingleton<TodosService>();

/*builder.Services.AddDbContext<TodoDb>(opt => opt.UseInMemoryDatabase("TodoList"));
builder.Services.AddDatabaseDeveloperPageExceptionFilter();*/

builder.Services.AddCors();

builder.Services.AddControllers().AddJsonOptions(
        options => options.JsonSerializerOptions.PropertyNamingPolicy = null);

var app = builder.Build();

app.UseRouting();

app.UseCors(x => x
               .AllowAnyMethod()
               .AllowAnyHeader()
               .SetIsOriginAllowed(origin => true) // allow any origin
               .AllowCredentials()); // allow credentials

app.UseAuthentication();
app.UseAuthorization();

//app.UseCorsMiddleware(); // old class we used to enable CORS, not being used anymore

/*app.MapControllerRoute(
    name: "default",
    pattern: "{controller=Home}/{action=Index}/{id?}");*/

app.UseEndpoints(x => x.MapControllers());

app.UseHttpsRedirection();

app.MapGet("/getall", () =>
{
    string data = JsonConvert.SerializeObject(
        new List<Todo>()
        {
            new Todo {
                title = "Presentatie",
                subject = "Cloud Computing",
                description = "A presentation about the seen things.",
                deadline = "6 december",
            },
            new Todo {
                title = "IoT verslag MQTT",
                subject = "IoT systems 2",
                description = "Verslag over de gemaakte code",
                deadline = "Vrijdagavond 10 december"
            },
            new Todo {
                title = "Tussentijds verslag angular app",
                subject = "Mobile IoT Dev",
                description = "Tussentijds verslag, nog geen idee waarover dit zou moeten gaan",
                deadline = "13 december 23h59",}
            }
    );
    return data;
});

app.Run();
